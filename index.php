<?php

include_once("config.php");

$resultado = mysqli_query($mysqli, "SELECT * FROM usuarios ORDER BY id DESC");

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

    <div class="container">

        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="nuevo.php">Nuevo</a>
            </li>
        </ul>

        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Año</th>
                    <th>Email</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while($fila = mysqli_fetch_array($resultado)) {
                ?>
                <tr>
                    <td><?= $fila['nombre'] ?></td>
                    <td><?= $fila['apellido'] ?></td>
                    <td><?= $fila['email'] ?></td>
                    <td>
                        <a href="perfil.php?id=<?= $fila['id'] ?>" class="btn btn-primary">Ver Perfil</a>
                        <a href="editar.php?id=<?= $fila['id'] ?>" class="btn btn-success">Editar</a>
                        <a href="eliminar.php?id=<?= $fila['id'] ?>" class="btn btn-danger" onClick="return confirm('Estas seguro de eliminar?')">Eliminar</a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>

    </div>
    
</body>
</html>