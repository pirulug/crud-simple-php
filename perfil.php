<?php
include "config.php"; //incluimos la configuracion de la base de datos que esta en config.php

if (isset($_GET['id'])) {
    
    $id = $_GET['id'];

    $resultado = mysqli_query($mysqli, "SELECT * FROM usuarios WHERE id=$id");

    while($fila = mysqli_fetch_array($resultado))
    {
        $id = $fila['id'];
        $nombre = $fila['nombre'];
        $apellido = $fila['apellido'];
        $email = $fila['email'];
    }
    
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

    <div class="container">

        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="nuevo.php">Nuevo</a>
            </li>
        </ul>

        <h5>Perfil</h5>
        <table class="table">
            <tbody>
                <tr>
                    <th class="text-right">Nombre: </th>
                    <td><?= $nombre ?></td>
                </tr>
                <tr>
                    <th class="text-right">Apellidos: </th>
                    <td><?= $apellido ?></td>
                </tr>
                <tr>
                    <th class="text-right">Email: </th>
                    <td><?= $email ?></td>
                </tr>
            </tbody>
        </table>

        <div class="text-center">
            <a href="editar.php?id=<?= $id ?>" class="btn btn-success">Editar</a>
            <a href="eliminar.php?id=<?= $id ?>" class="btn btn-danger">Eliminar</a>
        </div>

    </div>
    
</body>
</html>

<?php
}else{
    echo '<script>window.location.href = "index.php";</script>';
}
?>