<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

    <div class="container">

        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="nuevo.php">Nuevo</a>
            </li>
        </ul>

        <h5>Nuevo</h5>

        <?php

            include "config.php"; //incluimos la configuracion de la base de datos que esta en config.php

            if(isset($_POST['enviar_reg'])){
                $nombre = mysqli_real_escape_string($mysqli, $_POST['nombre_reg']);
                $apellido = mysqli_real_escape_string($mysqli, $_POST['apellido_reg']);
                $email = mysqli_real_escape_string($mysqli, $_POST['email_reg']);
                
                /* ----- revisar si no vienen campos vacios -----*/
                if(empty($nombre) || empty($apellido) || empty($email)) {
                            
                    if(empty($nombre)) {
                    echo "<div class='alert alert-danger'>El campo nombre esta vacio.</div>";
                    }
                    
                    if(empty($apellido)) {
                        echo "<div class='alert alert-danger'>El campo apellido esta vacio.</div>";
                    }
                    
                    if(empty($email)) {
                        echo "<div class='alert alert-danger'>El campo email esta vacio.</div>";
                    }
                    
                } else { 
                    /* ----- insertar en los campos de la base de datos -----*/
                    $result = mysqli_query($mysqli, "INSERT INTO usuarios(nombre,apellido,email) VALUES('$nombre','$apellido','$email')");
                    
                    echo "<div class='alert alert-success'>Datos agregados con éxito.";
                    echo "<a href='index.php' >Ver resultado</a></div>";
                }
            }

        ?>


        
        <form action="" method="post">
            <input class="form-control" placeholder="Nombre" type="text" name="nombre_reg" id=""><br>
            <input class="form-control" placeholder="Apellido" type="text" name="apellido_reg" id=""><br>
            <input class="form-control" placeholder="Email" type="text" name="email_reg" id=""><br>

            <div class="btn-group">
                <input class="btn btn-primary" placeholder="Email" type="submit" name="enviar_reg" id="">
            </div>
        </form>

    </div>
    
</body>
</html>